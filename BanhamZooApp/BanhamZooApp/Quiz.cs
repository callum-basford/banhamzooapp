﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace BanhamZooApp
{
    public class Quiz : INotifyPropertyChanged
    {
        public Quiz(string name)
        {
            Questions = new ObservableCollection<Question>();
            Name = name;
            Total = 0;
            NumbQuestions = 0;
        }

        public string Name
        {
            get; set;
        }

        public int NumbQuestions
        {
            get; set;
        }

        public int Total
        {
            get;
            private set;
        }

        public string Result
        {
            get;
            private set;
        }

        public void SetResult(int total, int questions)
        {
            Result = String.Format("{0}/{1}",total, questions);
            Debug.WriteLine(Result);
            OnPropertyChanged("Result");
        }

        public ObservableCollection<Question> Questions
        {
            get; set;
        }

        public void AddQuestion(Question q)
        {
            Questions.Add(q);
            NumbQuestions++;
            SetResult(Total, NumbQuestions);
        }

        public void AddQuestions(ObservableCollection<Question> qs)
        {
            foreach (var q in qs)
            {
                Questions.Add(q);
                NumbQuestions++;
                SetResult(Total, NumbQuestions);
            }
        }

        public void Increment()
        {
            Total++;
            SetResult(Total, NumbQuestions);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
