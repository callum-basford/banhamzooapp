﻿using System.Diagnostics;
using ImageCircle.Forms.Plugin.Abstractions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BanhamZooApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AnimalPage : ContentPage
    {
        private static AnimalPage _instance;
        private StackLayout stacklay;
        private ScrollView scrollview;

        public static Animal Animal
        {
            get; set;
        } = new Animal();

        private AnimalPage()
        {
            InitializeComponent();
        }

        public static AnimalPage GetAnimalPage()
        {
            if (_instance == null)
            {
                _instance = new AnimalPage();
                _instance.SetAnimal();
                return _instance;
            }
            else
            {
                _instance.SetAnimal();
                return _instance;
            }
        }

        private void SetAnimal()
        {
            scrollview = new ScrollView();
            stacklay = new StackLayout();
            var name = new Label()
            {
                Text = $"{Animal.Name}",
                FontSize = 28,
                TextColor = Color.FromHex(Application.Current.Resources["BanhamYellowHex"].ToString()),
                HorizontalTextAlignment = TextAlignment.Center,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                FontFamily = Device.OnPlatform(
                    Application.Current.Resources["iOSFont"].ToString(),
                    Application.Current.Resources["AndroidFont"].ToString(),
                    null
                    )
            };

            var image = new CircleImage()
            {
                Source = $"{Animal.Image}",
                HorizontalOptions = LayoutOptions.Center,
                BorderColor = Color.White,
                BorderThickness = 3,
                HeightRequest = 200,
                WidthRequest = 200,
                Aspect = Aspect.AspectFill
            };

            var more = new Button()
            {
                Text = "More info",
                FontSize = 14,
                TextColor = Color.White,
                WidthRequest = 125,
                BackgroundColor = Color.FromHex(Application.Current.Resources["BanhamYellowHex"].ToString()),
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                FontFamily = Device.OnPlatform(
                    Application.Current.Resources["iOSFont"].ToString(),
                    Application.Current.Resources["AndroidFont"].ToString(),
                    null
                ),
                Margin = 10
            };

            var less = new Button()
            {
                Text = "Fact file",
                FontSize = 14,
                TextColor = Color.White,
                WidthRequest = 125,
                BackgroundColor = Color.FromHex(Application.Current.Resources["BanhamYellowHex"].ToString()),
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                FontFamily = Device.OnPlatform(
                    Application.Current.Resources["iOSFont"].ToString(),
                    Application.Current.Resources["AndroidFont"].ToString(),
                    null
                ),
                Margin = 10
            };

            var factFile = new Label()
            {
                Text = $"Scientific Name: {Animal.ScienceName}\n\n" +
                       $"Top Fact: {Animal.Fact}\n\n" +
                       $"IUCN Status: {Animal.Status}\n\n" +
                       $"Native to: {Animal.NativeTo}\n\n" +
                       $"Size (length): {Animal.Size}\n\n" +
                       $"Diet: {Animal.Diet}\n\n" +
                       $"Young: {Animal.Young}",
                FontSize = 18,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                FontFamily = Device.OnPlatform(
                    Application.Current.Resources["iOSFont"].ToString(),
                    Application.Current.Resources["AndroidFont"].ToString(),
                    null
                    ),
                Margin = new Thickness(20)
            };

            var desc = new Label()
            {
                Text = Animal.Desc,
                FontSize = 18,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                HorizontalTextAlignment = TextAlignment.Center,
                FontFamily = Device.OnPlatform(
                    Application.Current.Resources["iOSFont"].ToString(),
                    Application.Current.Resources["AndroidFont"].ToString(),
                    null
                    ),
                Margin = new Thickness(20)
            };

            more.Clicked += (sender, args) =>
            {
                stacklay.Children.Remove(factFile);
                stacklay.Children.Remove(more);
                stacklay.Children.Add(less);
                stacklay.Children.Add(desc);
                scrollview.Content = stacklay;
                _instance.Content = scrollview;
            };

            less.Clicked += (sender, args) =>
            {
                stacklay.Children.Remove(desc);
                stacklay.Children.Remove(less);
                stacklay.Children.Add(more);
                stacklay.Children.Add(factFile);
                scrollview.Content = stacklay;
                _instance.Content = scrollview;
            };
            Debug.WriteLine("SETDONE");
            stacklay.Children.Add(name);
            stacklay.Children.Add(image);
            stacklay.Children.Add(more);
            stacklay.Children.Add(factFile);
            //stacklay.Children.Add(desc);
            scrollview.Content = stacklay;
            _instance.Content = scrollview;
            
        }

        
    }
}
