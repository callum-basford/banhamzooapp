﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace BanhamZooApp
{
    public class Event
    {
        public string Name
        {
            get;
            set;
        }

        [XmlIgnore()]
        public DateTime dateTime
        {
            get;
            set;
        }

        public string DayAndTime
        {
            get { return DayAndTime; }
            set
            {
                dateTime = DateTime.ParseExact(value, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
            }
        }

        public Event()
        {
        }

        public Event(string name, string dateTime)
        {
            this.Name = name;
            this.DayAndTime = dateTime;
        }
    }
}
