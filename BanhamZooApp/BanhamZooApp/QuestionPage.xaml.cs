﻿using System;
using System.Diagnostics;
using System.Linq;
using ImageCircle.Forms.Plugin.Abstractions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BanhamZooApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class QuestionPage : ContentPage
    {
        private static QuestionPage _instance;
        private Random rand;
        private Button answer;
        private Button option1;
        private Button option2;
        private StackLayout stacklay;

        public static Question Question
        {
            get; set;
        } = new Question();

        private QuestionPage()
        {
            InitializeComponent();
        }

        public static QuestionPage GetQuestionPage()
        {
            if (_instance == null)
            {
                _instance = new QuestionPage();
                _instance.SetQuestion();
                return _instance;
            }
            else
            {
                _instance.SetQuestion();
                return _instance;
            }
        }

        private void SetQuestion()
        {
            stacklay = new StackLayout();

            var image = new CircleImage()
            {
                Source = $"{Question.Animal.Image}",
                HorizontalOptions = LayoutOptions.Center,
                BorderColor = Color.White,
                BorderThickness = 3,
                HeightRequest = 200,
                WidthRequest = 200,
                Aspect = Aspect.AspectFill,
                Margin = 20
            };

            var question = new Label()
            {
                Text = $"{Question.QuestionString}",
                FontSize = 28,
                TextColor = Color.White,
                HorizontalTextAlignment = TextAlignment.Center,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                FontFamily = Device.OnPlatform(
                    Application.Current.Resources["iOSFont"].ToString(),
                    Application.Current.Resources["AndroidFont"].ToString(),
                    null
                )
            };

            answer = new Button()
            {
                HorizontalOptions = LayoutOptions.Fill,
                Text = $"{Question.Answer}",
                FontSize = 14,
                BackgroundColor = Color.FromHex("#f2b216"),
                TextColor = Color.White,
                FontFamily = Device.OnPlatform(
                    Application.Current.Resources["iOSFont"].ToString(),
                    Application.Current.Resources["AndroidFont"].ToString(),
                    null
                )
            };

            option1 = new Button()
            {
                HorizontalOptions = LayoutOptions.Fill,
                Text = $"{Question.Option1}",
                FontSize = 14,
                BackgroundColor = Color.FromHex("#f2b216"),
                TextColor = Color.White,
                FontFamily = Device.OnPlatform(
                    Application.Current.Resources["iOSFont"].ToString(),
                    Application.Current.Resources["AndroidFont"].ToString(),
                    null
                )
            };

            if (Question.Option2 != null)
            {
                option2 = new Button()
                {
                    HorizontalOptions = LayoutOptions.Fill,
                    Text = $"{Question.Option2}",
                    FontSize = 14,
                    BackgroundColor = Color.FromHex("#f2b216"),
                    TextColor = Color.White,
                    FontFamily = Device.OnPlatform(
                     Application.Current.Resources["iOSFont"].ToString(),
                     Application.Current.Resources["AndroidFont"].ToString(),
                     null
                 )
                };
                option2.Clicked += Option2Clicked;
            }
            answer.Clicked += AnswerClicked;
            option1.Clicked += Option1Clicked;
            

            var numbs = Enumerable.Range(0, 3);
            rand = new Random();
            var shuffledNumbs = numbs.OrderBy(a => rand.Next());
            var enumer = shuffledNumbs.GetEnumerator();

            Button[] buttons = new Button[3];
            buttons[0] = answer;
            buttons[1] = option1;
            buttons[2] = option2;

            stacklay.Children.Add(image);
            stacklay.Children.Add(question);
            foreach (var nums in shuffledNumbs)
            {
                enumer.MoveNext();
                if (enumer.Current == 2 && Question.Option2 == null)
                {

                }
                else
                {
                    stacklay.Children.Add(buttons[enumer.Current]);
                }
            }
            enumer.Dispose();

            _instance.Content = stacklay;
        }

        private void AnswerClicked(object sender, EventArgs e)
        {
            QuizPage.Quiz.Increment();
            answer.BackgroundColor = Color.Lime;
            Question._backgroundColour = Color.Lime;
            HandleClicked();
        }

        private void Option1Clicked(object sender, EventArgs e)
        {
            option1.BackgroundColor = Color.Red;
            answer.BackgroundColor = Color.Lime;
            Question._backgroundColour = Color.Red;
            HandleClicked();
        }

        private void Option2Clicked(object sender, EventArgs e)
        {
            option2.BackgroundColor = Color.Red;
            answer.BackgroundColor = Color.Lime;
            Question._backgroundColour = Color.Red;
            HandleClicked();
        }

        private void HandleClicked()
        {
            Question._answerable = false;
            var nextButton = new Button()
            {

                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.End,
                FontSize = 18,
                WidthRequest = 80,
                BackgroundColor = Color.FromHex("#f2b216"),
                TextColor = Color.White,
                FontFamily = Device.OnPlatform(
                Application.Current.Resources["iOSFont"].ToString(),
                Application.Current.Resources["AndroidFont"].ToString(),
                null
            )
            };

            String buttonText = "Next";

            if (NextQuestion(QuizPage.Quiz.Questions.IndexOf(Question)) == -1)
            {
                buttonText = "Finish";
                nextButton.Clicked += (sender, args) =>
                {
                    Navigation.PopAsync(false);
                };
            }
            else
            {
                nextButton.Clicked += (sender, args) =>
                {
                    Question = QuizPage.Quiz.Questions[NextQuestion(QuizPage.Quiz.Questions.IndexOf(Question))];
                    SetQuestion();
                };
            }
            nextButton.Text = buttonText;
            
            answer.IsEnabled = false;
            option1.IsEnabled = false;
            if (option2 != null)
            {
                option2.IsEnabled = false;
            }
            stacklay.Children.Add(nextButton);

        }

        private int NextQuestion(int pos)
        {
            if (pos == QuizPage.Quiz.Questions.Count-1)
            {
                pos = -1;
            }
            for (int i = pos+1; i < QuizPage.Quiz.Questions.Count; i++)
            {
                if (QuizPage.Quiz.Questions[i]._answerable)
                {
                    
                    return i;
                }
            }
            return -1;
        }
    }
}
