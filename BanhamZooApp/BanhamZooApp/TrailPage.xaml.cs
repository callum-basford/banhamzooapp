﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BanhamZooApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TrailPage : ContentPage
    {
        private static TrailPage _instance;
        private Trail Trail;

        private TrailPage()
        {
            Title = "Map";
            InitializeComponent();
        }

        public static TrailPage GeTrailPage()
        {
            if (_instance == null)
            {
                _instance = new TrailPage();
                return _instance;
            }
            else
            {
                return _instance;
            }
        }
        
        public void SetTrail(Trail t)
        {
            _instance.Trail = t;

            var im = new ZoomImage()
            {
                Source = t.MapImage,
                Aspect = Aspect.AspectFit
            };
            Device.OnPlatform(
                () =>
                {
                    im.AnchorX = 0.5;
                    im.AnchorY = 0.5;
                }
            , 
                () =>
                {
                im.AnchorX = 0;
                im.AnchorY = 0;
                });
            _instance.Content = im;
        }
    }
}
