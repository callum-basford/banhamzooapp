﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BanhamZooApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CategoriesPage : ContentPage
    {

        private static CategoriesPage _instance;

        private CategoriesPage()
        {
            InitializeComponent();
        }

        public static CategoriesPage GetCategoriesPage()
        {
            if (_instance == null)
            {
                _instance = new CategoriesPage();
                return _instance;
            }
            else
            {
                return _instance;
            }
        }

        private void VulnerableTapped(object sender, EventArgs e)
        {
            HandleClicked("VU");
        }

        private void EndangeredTapped(object sender, EventArgs e)
        {
            HandleClicked("EN");
        }

        private void CriticalTapped(object sender, EventArgs e)
        {
            HandleClicked("CR");
        }

        private void AllTapped(object sender, EventArgs e)
        {
            HandleClicked("ALL");
        }

        private async void HandleClicked(String list)
        {
            AnimalsPage.GetAnimalsPage().FilterStatus(list);
            if (list == "ALL")
            {
                TrailPage.GeTrailPage().SetTrail(MainPage.GetMainPage().Trails[0]);
            }
            else
            {
                foreach (var VARIABLE in MainPage.GetMainPage().Trails)
                {
                    if (VARIABLE.Status.Equals(list))
                    {
                        TrailPage.GeTrailPage().SetTrail(VARIABLE);
                    }
                }
            }
            await Navigation.PushAsync(new TabbedPage()
            {
                Children = {AnimalsPage.GetAnimalsPage() ,TrailPage.GeTrailPage(), TrailPage.GeTrailPage()}
            });
        }
    }
}
