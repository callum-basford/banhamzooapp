﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BanhamZooApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class QuizPage : ContentPage
    {
        private static QuizPage _instance;
        
        public static Quiz Quiz
        {
            get; set;
        }

        public static QuizPage GetQuizPage()
        {
            if (_instance == null)
            {
                _instance = new QuizPage();
                _instance.QuestionsList.ItemsSource = Quiz.Questions;
                return _instance;
            }
            else
            {
                _instance.QuestionsList.ItemsSource = Quiz.Questions;
                return _instance;
            }
        }

        private QuizPage()
        {
            InitializeComponent();
        }

        private async void QuestionsList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            QuestionPage.Question = (Question) e.Item;
            await Navigation.PushAsync(QuestionPage.GetQuestionPage());
        }

        private void QuestionsList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ((ListView)sender).SelectedItem = null;
        }
    }
}
