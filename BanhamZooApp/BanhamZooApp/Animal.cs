﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace BanhamZooApp
{
    public class Animal : BindableObject, IComparable
    {
        public string Name
        {
            get;
            set;
        }

        public string ScienceName
        {
            get;
            set;
        }

        public int Latitude
        {
            get; set;
        }

        public int Longitude
        {
            get; set;
        }

        public string Desc
        {
            get;
            set;
        }

        public string Size
        {
            get;
            set;
        }

        public string Status
        {
            get;
            set;
        }

        public string NativeTo
        {
            get;
            set;
        }

        public string Diet
        {
            get;
            set;
        }

        public string Young
        {
            get;
            set;
        }

        public string Fact
        {
            get;
            set;
        }

        public string LookFor
        {
            get;
            set;
        }

        public string Image
        {
            get; set;
        }

        public int CompareTo(Object obj)
        {
            Animal b = (Animal) obj;
            return String.Compare(this.Name, b.Name, StringComparison.OrdinalIgnoreCase);
        }
    }
}
