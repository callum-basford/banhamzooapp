﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BanhamZooApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TrailsPage : ContentPage
    {
        private static TrailsPage _instance;
        private ObservableCollection<Trail> Trails;
        
        private TrailsPage()
        {
            InitializeComponent();
        }

        public static TrailsPage GetTrailsPage()
        {
            if (_instance == null)
            {
                _instance = new TrailsPage();
                return _instance;
            }
            else
            {
                return _instance;
            }
        }
    }
}
