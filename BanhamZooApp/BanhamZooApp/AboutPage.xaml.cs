﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BanhamZooApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutPage : ContentPage
    {
        public static AboutPage _instance;

        private AboutPage()
        {
            InitializeComponent();
        }

        public static AboutPage GetAboutPage()
        {
            if (_instance == null)
            {
                _instance = new AboutPage();
                return _instance;
            }
            else
            {
                return _instance;
            }
        }
    }
}
