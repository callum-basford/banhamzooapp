﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BanhamZooApp
{
    public class Trail
    {
        public string Name
        {
            get; set;
        }

        public string MapImage
        {
            get; set;
        }

        public ObservableCollection<Animal> Animals
        {
            get; set;
        }

        public string Status
        {
            get; set;
        }

    }
}
