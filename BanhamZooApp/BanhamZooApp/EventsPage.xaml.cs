﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BanhamZooApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EventsPage : ContentPage
    {
        private static EventsPage _instance;
        
        private EventsPage()
        {
            InitializeComponent();
            var scroll = new ScrollView();
            scroll.Content = new Label()
            {
                Text = "hello"
            };
            this.Content = scroll;

            //GetDocumentAsync();
        }

        public static EventsPage GetWhatsOnPage()
        {
            if(_instance == null)
            {
                _instance = new EventsPage();
                
                return _instance;
            }
            else
            {
                return _instance;
            }
        }

        //public async Task<string> GetDocumentAsync()
        //{
        //    var client = new HttpClient();
        //    var response = await client.GetAsync("https://www.dropbox.com/s/6wb7cakw236k1lh/homePageTest.xml?dl=1");
        //    var scroll = new ScrollView();
        //    scroll.Content = new Label()
        //    {
        //        Text = response.Content.ReadAsStringAsync().Result
        //    };
        //    this.Content = scroll;
        //    var responseString = response.Content.ReadAsStringAsync().Result;
        //    return responseString;
        //}

    }
}
