﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Xamarin.Forms;

namespace BanhamZooApp
{
    public class Question : INotifyPropertyChanged
    {
        public string Answer
        {
            get;
            set;
        }

        private bool Answerable { get; set; } = true;

        private Color BackgroundColour { get; set; } = (Color)Application.Current.Resources["BanhamGreen"];

        public Color _backgroundColour
        {
            get { return BackgroundColour; }
            set
            {
                BackgroundColour = value;
                OnPropertyChanged("_backgroundColour");
            }
        }

        public bool _answerable
        {
            get { return Answerable; }
            set
            {
                Answerable = value;
                OnPropertyChanged("_answerable");
            }
        }

        public string Option1
        {
            get;
            set;
        }

        public string Option2
        {
            get;
            set;
        }

        public string QuestionString
        {
            get;
            set;
        }

        public Animal Animal
        {
            get;
            set;
        }

        private string _AnimalString { get; set; }

        public string AnimalString
        {
            get
            {
                return _AnimalString;
            }
            set
            {
                _AnimalString = value;
                //SetAnimal(value);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
