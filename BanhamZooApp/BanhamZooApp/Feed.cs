﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BanhamZooApp
{
    public class Feed : Event
    {
        public string Animal
        {
            get;
            set;
        }

        public Feed() : base()
        {
            
        }

        public Feed(string name, string dateTime, string animal) : base(name, dateTime)
        {
            this.Animal = animal;
        }
    }
}
