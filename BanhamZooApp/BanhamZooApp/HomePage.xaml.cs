﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Net;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BanhamZooApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage : ContentPage
    {

        private static HomePage _instance;

        private HomePage()
        {
            InitializeComponent();
        }

        public static HomePage GetHomePage()
        {
            if(_instance == null)
            {
                _instance = new HomePage();
                return _instance;
            }
            else
            {
                return _instance;
            }
        }

        private async void WhatsOnTapped(object sender, EventArgs e)
        {
            var a = new WebView()
            {
                Source = "http://www.banhamzoo.co.uk/today-at-banham-zoo/"
            };
           

            await Navigation.PushAsync(new ContentPage()
            {
                Content = a
            });
        }


        private async void AboutTapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(AboutPage.GetAboutPage());
        }
        
        private async  void ConservationTapped(object sender, EventArgs e)
        {
            
            
            await Navigation.PushAsync(new ContentPage(){
                Content = new WebView()
                {
                    Source = "http://www.banhamzoo.co.uk/zsea/conservation/"
                }
                });
        }
    }
}
