﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;
using Plugin.Geolocator;
using Xamarin.Forms;

namespace BanhamZooApp
{
    public partial class MainPage
    {
        private static MainPage _instance;
        private HomePage _hp = HomePage.GetHomePage();
        private AnimalsPage _ap = AnimalsPage.GetAnimalsPage();
        private QuizzesPage _qp = QuizzesPage.GetQuizzesPage();
        private CategoriesPage _cp = CategoriesPage.GetCategoriesPage();
        private TrailsPage _tp = TrailsPage.GetTrailsPage();
        private TrailPage _trailPage = TrailPage.GeTrailPage();
        
        private List<Feed> Feeds
        {
            get;
            set;
        }

        public ObservableCollection<Animal> Animals
        {
            get;
            set;
        }

        public ObservableCollection<Question> Questions
        {
            get;
            set;
        }

        public ObservableCollection<Trail> Trails
        {
            get;
            set;
        }

        private MainPage()
        {
            InitializeComponent();
            Detail = new NavigationPage(_hp)
            {
                BarBackgroundColor = (Color)Application.Current.Resources["BanhamDark"]
            };

            var assembly = typeof(MainPage).GetTypeInfo().Assembly;
            var stream = assembly.GetManifestResourceStream("BanhamZooApp.Events.xml");
            using (var reader = new StreamReader(stream))
            {
                try
                {
                    XmlRootAttribute xRoot = new XmlRootAttribute();
                    xRoot.ElementName = "Events";
                    xRoot.IsNullable = true;
                    var serializer = new XmlSerializer(typeof(List<Feed>), xRoot);
                    Feeds = (List<Feed>) serializer.Deserialize(reader);
                }
                catch (Exception e)
                {
                    throw e.InnerException;
                }
            }
            
            stream = assembly.GetManifestResourceStream("BanhamZooApp.Animals.xml");
            using (var reader = new StreamReader(stream))
            {
                try
                {
                    XmlRootAttribute xRoot = new XmlRootAttribute();
                    xRoot.ElementName = "AnimalList";
                    xRoot.IsNullable = true;
                    var serializer = new XmlSerializer(typeof(ObservableCollection<Animal>), xRoot);
                    Animals = (ObservableCollection<Animal>)serializer.Deserialize(reader);
                }
                catch (Exception e)
                {
                    throw e.InnerException;
                }
            }
            foreach (var a in Animals)
            {
                _ap.Animals.Add(a);
            }

            stream = assembly.GetManifestResourceStream("BanhamZooApp.Questions.xml");
            using (var reader = new StreamReader(stream))
            {
                try
                {
                    XmlRootAttribute xRoot = new XmlRootAttribute();
                    xRoot.ElementName = "QuestionList";
                    xRoot.IsNullable = true;
                    var serializer = new XmlSerializer(typeof(ObservableCollection<Question>), xRoot);
                    Questions = (ObservableCollection<Question>)serializer.Deserialize(reader);
                }
                catch (Exception e)
                {
                    throw e.InnerException;
                }
            }

            stream = assembly.GetManifestResourceStream("BanhamZooApp.Trails.xml");
            using (var reader = new StreamReader(stream))
            {
                try
                {
                    XmlRootAttribute xRoot = new XmlRootAttribute();
                    xRoot.ElementName = "TrailList";
                    xRoot.IsNullable = true;
                    var serializer = new XmlSerializer(typeof(ObservableCollection<Trail>), xRoot);
                    Trails = (ObservableCollection<Trail>)serializer.Deserialize(reader);
                }
                catch (Exception e)
                {
                    throw e.InnerException;
                }
            }

            foreach (var q in Questions)
            {
                _qp.Questions.Add(q);
            }

            Quiz VUQuiz = new Quiz("Vulnerable Animals");
            Quiz ENQuiz = new Quiz("Endangered Animals");
            Quiz CRQuiz = new Quiz("Critically Endangered Animals");
            foreach (var q in Questions)
            {
                foreach (var VARIABLE in Animals)
                {
                    if (q.AnimalString == VARIABLE.Name)
                    {
                        q.Animal = VARIABLE;
                    }
                }
            }

            foreach (var VARIABLE in Questions)
            {
                //Debug.WriteLine(VARIABLE.Animal.Name);
                if (VARIABLE.Animal.Status == "VU")
                {
                    VUQuiz.AddQuestion(VARIABLE);
                }
                if (VARIABLE.Animal.Status == "EN")
                {
                    ENQuiz.AddQuestion(VARIABLE);
                }
                if (VARIABLE.Animal.Status == "CR")
                {
                    CRQuiz.AddQuestion(VARIABLE);
                }
            }

            _qp.Quizzes.Add(VUQuiz);
            _qp.Quizzes.Add(ENQuiz);
            _qp.Quizzes.Add(CRQuiz);
           // GetLoc();
           Debug.WriteLine($"Trail {Trails[0].Name}");
        }

        public async void GetLoc()
        {
            try
            {
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 50;

                var position = await locator.GetPositionAsync(10000);
                if (position == null)
                    return;

                Debug.WriteLine("Position Status: {0}", position.Timestamp);
                Debug.WriteLine("Position Latitude: {0}", position.Latitude);
                Debug.WriteLine("Position Longitude: {0}", position.Longitude);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Unable to get location, may need to increase timeout: " + ex);
            }
        }

        public static MainPage GetMainPage()
        {
            if(_instance == null)
            {
                _instance = new MainPage();
                return _instance;
            }
            else
            {
                return _instance;
            }
        }
        
        public void HandleClicked(Page p)
        {
            Detail = new NavigationPage(p)
            {
                BarBackgroundColor = (Color)Application.Current.Resources["BanhamDark"],
            };
            IsPresented = false;
        }
                

        private void Home_Clicked(object sender, EventArgs e)
        {
            HandleClicked(_hp);
        }

        private void Trails_Clicked(object sender, EventArgs e)
        {
            HandleClicked(_cp);
        }

        private void Quizzes_Clicked(object sender, EventArgs e)
        {
            HandleClicked(_qp);
        }

        public void SetDetail(Page p)
        {
            Detail = p;
        }
        
    } 
}
