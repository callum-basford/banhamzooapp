﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BanhamZooApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AnimalsPage : ContentPage
    {
        private static AnimalsPage _instance;

        public ObservableCollection<Animal> Animals { get; set; }
        private ObservableCollection<Animal> List { get; set; }
        private IOrderedEnumerable<Animal> ViewableList { get; set; }

        private AnimalsPage()
        {
            InitializeComponent();
            Animals = new ObservableCollection<Animal>();
            List = new ObservableCollection<Animal>();
            AnimalsList.ItemsSource = Animals;
        }

        public static AnimalsPage GetAnimalsPage()
        {
            if (_instance == null)
            {
                _instance = new AnimalsPage();
                return _instance;
            }
            else
            {
                return _instance;
            }
        }

        public void FilterStatus(String filter)
        {
            List.Clear();
            ViewableList = List.OrderBy(x => x.Name);
            if (filter.Equals("ALL"))
            {
                foreach (var VARIABLE in Animals)
                {
                    List.Add((VARIABLE));
                }
            }
            else
            {
                foreach (var VARIABLE in Animals)
                {
                    if (VARIABLE.Status.Equals(filter))
                    {
                        List.Add(VARIABLE);
                    }
              }
            }
            ViewableList = List.OrderBy(x => x.Name);
            AnimalsList.ItemsSource = ViewableList;
        }

        private async void AnimalsList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            Debug.WriteLine(e.Item.ToString());
            AnimalPage.Animal = (Animal)e.Item;
            await Navigation.PushAsync(AnimalPage.GetAnimalPage());
        }

        private void AnimalsList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ((ListView)sender).SelectedItem = null;
        }
    }
}
