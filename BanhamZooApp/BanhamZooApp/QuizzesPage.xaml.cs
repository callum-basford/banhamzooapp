﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BanhamZooApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class QuizzesPage : ContentPage
    {
        private static QuizzesPage _instance;

        public ObservableCollection<Quiz> Quizzes { get; set; }
        public ObservableCollection<Question> Questions { get; set; }

        private QuizzesPage()
        {
            InitializeComponent();
            Quizzes = new ObservableCollection<Quiz>();
            Questions = new ObservableCollection<Question>();
            QuizzesList.ItemsSource = Quizzes;
        }

        public static QuizzesPage GetQuizzesPage()
        {
            if (_instance == null)
            {
                _instance = new QuizzesPage();
                return _instance;
            }
            else
            {
                return _instance;
            }
        }

        private async void QuizzesList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            QuizPage.Quiz = (Quiz)e.Item;
            await Navigation.PushAsync(QuizPage.GetQuizPage());
        }

        private void QuizzesList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ((ListView)sender).SelectedItem = null;
        }
    }
}
